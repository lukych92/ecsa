# Endogenous Configuration Space Approach (ECSA) #
## Version: 1.1.1 ##
This python package provides set of algorithms for motion planning and trajectory tracking problem based on Endogenous Configuration Space Approach. You can find more information about methodology, usage, and development in folder 'doc'.

---
### How to run examples ###

I. Download the package from python repository
```
pip install ecsa
```

II. Download CasADi framework in version 3.0.0. Thus you should download and decompress the following [file](https://sourceforge.net/projects/casadi/files/CasADi/3.0.0/linux/casadi-py27-np1.9.1-v3.0.0.tar.gz/download) in your home directory

III. Clone the repository and run examples, e.g.
```
python ecsa/examples/DemoPseudoinverse.py
```
---
Package dependencies:

* python 2.7
* casadi 3.0.0
* numpy 1.12.0
* matplotlib 2.0.0
* multiprocessing 0.70a1