import os, sys
sys.path.append(os.path.join(os.path.expanduser('~'), 'casadi-py27-np1.9.1-v3.0.0'))
from casadi import *

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle
from ECSA.Algorithm import LiftedNewton
import ECSA.API.Visualization

if __name__ == '__main__':
    print "*****Start"

    opts = {}
    opts['T'] = 3.0
    opts['gamma0'] = 0.7
    opts['TOL'] = 10**(-4)
    opts['k_max'] = 200

    opts['q0'] = [10,0,pi/2]
    opts['x0'] = []
    opts['yd'] = [[10,10],[5,5],[0,10],[0,5],[0,0,pi/2]]

    la1 = [0.5, 0.25, -0.25, 0.1, 0.1]
    la2 = [-0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01]
    opts['lambda0'] = vertcat(la1, la2)
    opts['lambda'] = [[len(la1),0],[len(la2),0]]

    opts['viz'] = [False ,False,0.2]

    fourier = Fourier(opts)
    monocycle = Monocycle(fourier)
    ecsa = LiftedNewton(monocycle,opts)

    print "******ECSA Lifted Newton"
    out = ecsa.run()
    print "******End ECSA Lifted Newton"


    if opts['viz'][0] == False:
        ECSA.API.Visualization.showPath(out,opts)
    ECSA.API.Visualization.showTrajectory(out)
    ECSA.API.Visualization.showError(out)
    ECSA.API.Visualization.showControl(out)

    print "*****End"