import os, sys
sys.path.append(os.path.join(os.path.expanduser('~'), 'casadi-py27-np1.9.1-v3.0.0'))
from casadi import *

import time
from datetime import datetime
import matplotlib.pyplot as plt

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle, PioneerDynamics, KinematicCar
from ECSA.Algorithm import LiftedNewtonProritarian
import ECSA.API.Visualization


factor = 1

def frange(x, y, jump):
    while x < y:
        yield x
        x += jump



def yd(t):
    R = 1
    w = 0.25*factor
    orientation = (w * t) - np.pi / 2
    return vertcat(R * np.cos(orientation), R * np.sin(orientation) + R, orientation + np.pi / 2)


def generete_trajectory(opts,from_time,to_time):
    ## desired trajectory
    # print "---- time horizon",from_time, to_time
    ydt = yd(from_time)
    # opts['ydt'] = yd(0)
    for t in [x for x in frange(from_time+opts['dts'], to_time, opts['dts'])]:
        ydt = horzcat(ydt,yd(t))

    return ydt

    ## end desired trajectory


if __name__ == '__main__':
    print "*****Start"
    #
    opts = {}
    opts['gamma0'] = 0.7
    opts['TOL'] = 10**(-3)
    opts['k_max'] = 2
    opts['dts'] = 0.01

    opts['q0'] = [0,1,0,0,0,0,0]
    # opts['q0'] = [0,0.1,0,0]
    # opts['q0'] = [0,1,0]
    opts['x0'] = []
    opts['N'] = 2


    opts['yd'] = yd

    # plt.plot(opts['ydt'][0,:].T,opts['ydt'][1,:].T)
    # plt.show()
    opts['T_total'] = 25.12/factor


    la1 = [0.5, 0.01, 0.0, 0.0 ]
    la2 = [0.5, 0.01, 0.0, 0.0 ]
    opts['lambda0'] = vertcat(la1, la2)
    opts['lambda'] = [[len(la1),0],[len(la2),0]]

    opts['viz'] = [False,False,0.02]

    q = opts['q0']
    step = 100
    step_i = 1

    iterations = [int(x) for x in range(2512/factor)]
    # iterations = [int(x) for x in range(500)]
    velocity_linear = []
    velocity_angular = []


    max_time = 0
    # opts['ydt'] = generete_trajectory(opts,0,opts['T'])
    print "******ECSA Lifted Newton"
    start_time1 = time.time()
    for i in iterations:
        start_time = time.time()

        opts['T0'] = i*opts['dts']*step_i
        opts['T'] = (i*opts['dts']*step_i)+(step*opts['dts'])
        opts['ydt'] = generete_trajectory(opts,opts['T0'],opts['T'])
        # opts['ydt'] = generete_trajectory(opts,0,opts['T'])
        # opts['lambda0'] = vertcat(la1, la2)
        fourier = Fourier(opts)
        fourier.setTb(opts['T0'])
        # monocycle = Monocycle(fourier)
        monocycle = PioneerDynamics(fourier)
        # monocycle.m = monocycle.r + 100
        # monocycle_sim = PioneerDynamics(fourier)
        # opts['system_sim'] = monocycle_sim
        # monocycle = Monocycle(fourier)
        # monocycle = KinematicCar(fourier)
        ecsa = LiftedNewtonProritarian(monocycle,opts)
        out = ecsa.run()

        # print out['tgrid'][step_i],opts['T0']
        if i%50 == 0:
            print "***iter "+str(i)
        # print out['lambda']
        # print out['u']
        #     ECSA.API.Visualization.showPath(out, opts)
        # ECSA.API.Visualization.showControl(out)

        # print("--- %.2f ms ---" % ((time.time() - start_time)*1000))
        if ((time.time() - start_time)*1000) > max_time:
            max_time = ((time.time() - start_time)*1000)

        # opts['lambda0'] = ecsa.NumpyToCasadi(out['lambda'][0])
        opts['q0'] =  ecsa.CasadiToNumpy(out['q'][:,step_i])
        q = horzcat(q, opts['q0'].T)
        velocity_linear = horzcat(velocity_linear,out['u'][0,0])
        velocity_angular = horzcat(velocity_angular,out['u'][1,0])

    print "******End ECSA Lifted Newton"
    total_time = float(time.time() - start_time1)
    print("--- max_time %.2f ms ---" % max_time)
    print("--- total_time %.2f s ---" % total_time)

    opts['ydt'] = generete_trajectory(opts, 0, opts['T_total'])
    time = numpy.array([x for x in frange(0, opts['T_total'], opts['dts'])]).T

    reference_x = opts['ydt'][0,:].T
    reference_y = opts['ydt'][1,:].T
    reference_orientation = opts['ydt'][2,:].T
    actual_x = q[0,:].T
    actual_y = q[1,:].T
    actual_theta = q[2,:].T

    f = open('real_time_control_system_lifted_newton_prioritarian.txt', 'w')
    f.write("# Lifted Newton Prioritarian log \n")
    curr_time = datetime.now().strftime('%d-%m-%Y %H:%M:%S')
    f.write("# "+curr_time+" \n")
    f.write("# --- max_time "+str(max_time)+" ms ---\n")
    f.write("# --- total_time "+str(total_time)+" s ---\n")
    f.write("# Time horizon "+str(step*opts['dts'])+" \n")
    f.write("# gamma0 "+str(opts['gamma0'])+" \n")
    f.write("# TOL "+str(opts['TOL'])+" \n")
    f.write("# k_max "+str(opts['k_max'])+" \n")
    f.write("# dts "+str(opts['dts'])+" \n")
    f.write("# lambda0 "+str(opts['lambda0'])+" \n")
    f.write("# model mass + 1 "+str(monocycle)+" \n")
    for i in iterations:
        string_line = ' '.join(map(str,[time[i],
                                        actual_x[i],
                                        actual_y[i],
                                        actual_theta[i],
                                        reference_x[i],
                                        reference_y[i],
                                        reference_orientation[i],
                                        velocity_linear[i],
                                        velocity_angular[i]
                                        ]))
        f.write(string_line+'\n')

    f.close()

    # plt.figure(1)
    # plt.plot(q[0,:].T,q[1,:].T,'k',opts['ydt'][0,:].T,opts['ydt'][1,:].T,'k-.')
    # plt.plot(reference_x,reference_y,'k--',label='yd')
    # plt.plot(actual_x,actual_y,'k',label = 'y')
    # plt.plot(actual_x[0],actual_y[0],'ok', label="Initial position")
    # plt.title('Desired and realized path')
    # plt.legend(loc=3, numpoints=1)
    # plt.xlabel('x, xd [m]')
    # plt.ylabel('y, yd [m]')
    # plt.show()
    # plt.figure(2)
    # plt.plot(time,actual_x,'k',time,reference_x,'k-.')
    # plt.show()
    # plt.figure(3)
    # plt.plot(time,actual_y,'k',time,reference_y,'k-.')
    # plt.show()
    # plt.figure(4)
    # plt.plot(time, actual_theta, 'k', time, reference_theta, 'k-.')
    # plt.show()
    # plt.figure(5)
    # plt.plot(time,opts['ydt'][0,:].T-q[0,:].T,'k-')
    # plt.show()
    # plt.figure(6)
    # plt.plot(time,opts['ydt'][1,:].T-q[1,:].T,'k-')
    # plt.show()
    # plt.figure(7)
    # plt.plot(time,opts['ydt'][2,:].T-q[2,:].T,'k-')
    # plt.show()


    # if opts['viz'][0] == False:
    #     ECSA.API.Visualization.showPath(out,opts)
    # ECSA.API.Visualization.showTrajectory(out)
    # ECSA.API.Visualization.showError(out)
    # ECSA.API.Visualization.showControl(out)

    print "*****End"