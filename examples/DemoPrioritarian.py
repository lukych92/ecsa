import os, sys
sys.path.append(os.path.join(os.path.expanduser('~'), 'casadi-py27-np1.9.1-v3.0.0'))
from casadi import *

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle
from ECSA.Algorithm import Prioritarian
import ECSA.API.Visualization

if __name__ == '__main__':
    print "*****Start"

    opts = {}
    opts['T'] = 1.0
    opts['gamma0'] = 0.1
    opts['TOL'] = 10**(-2)
    opts['k_max'] = 200

    opts['q0'] = [10,0,pi/2]
    opts['x0'] = []
    opts['yd'] = [0,0,0]

    la1 = [0.5, 0.25, -0.25, 0.1, 0.1]
    la2 = [-0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01]
    opts['lambda0'] = vertcat(la1, la2)
    opts['lambda'] = [[len(la1),0],[len(la2),0]]

    bounds = {}
    ## bounds[variable] = [lb,up,alpha]]
    ## note: bounds are numerated from 0
    # bounds['u0'] = [-30,30,1]
    bounds['u1'] = [-10,10,1]

    opts['bounds'] = bounds

    opts['viz'] = [False,False,0.2]

    fourier = Fourier(opts)
    monocycle = Monocycle(fourier)
    ecsa = Prioritarian(monocycle,opts)

    print "******ECSA Prioritarian"
    out = ecsa.run()
    print "******End ECSA Prioritarian"


    if opts['viz'][0] == False:
        ECSA.API.Visualization.showPath(out,opts)
    ECSA.API.Visualization.showTrajectory(out)
    ECSA.API.Visualization.showError(out)
    ECSA.API.Visualization.showControl(out,bounds)

    print "*****End"
