import os, sys
sys.path.append(os.path.join(os.path.expanduser('~'), 'casadi-py27-np1.9.1-v3.0.0'))
from casadi import *

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle
from ECSA.System import ImbalancedSystem
from ECSA.Algorithm import Imbalanced
import ECSA.API.Visualization

if __name__ == '__main__':
    print "*****Start"

    opts = {}
    opts['T'] = 1.0
    opts['gamma0'] = 0.1
    opts['TOL'] = 10**(-1)
    opts['k_max'] = 200

    opts['q0'] = [10,0,pi/2]
    opts['x0'] = []
    opts['yd'] = [0,0,0]

    la1 = [0.5, 0.25, -0.25, 0.1, 0.1]
    la2 = [-0.5, 0.25, 0.25, 0.1, 0.1, 0.01, 0.01]
    opts['lambda0'] = vertcat(la1, la2)
    opts['lambda'] = [[len(la1),0],[len(la2),0]]

    bounds = {}
    ## bounds[variable] = [lb,up,alpha]]
    ## note: bounds are numerated from 0
    # bounds['q2'] = [-pi/2,pi/2,1]
    # bounds['u0'] = [-13,13,1]
    bounds['u1'] = [-12,12,1]

    opts['q0'] = numpy.hstack((opts['q0'],[0 for x in range(len(bounds))]))
    opts['viz'] = [False,False,0.2]

    fourier = Fourier(opts)
    monocycle = Monocycle(fourier)
    monocycleConstrained = ImbalancedSystem(monocycle,bounds)
    ecsa = Imbalanced(monocycleConstrained,fourier,opts)

    print "******ECSA Imbalanced"
    out = ecsa.run()
    print "******End ECSA Imbalanced"

    if opts['viz'][0] == False:
        ECSA.API.Visualization.showPath(out,opts)
    ECSA.API.Visualization.showTrajectory(out,bounds)
    ECSA.API.Visualization.showError(out)
    ECSA.API.Visualization.showControl(out,bounds)

    print "*****End"
