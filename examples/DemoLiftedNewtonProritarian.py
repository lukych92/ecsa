import os, sys
sys.path.append(os.path.join(os.path.expanduser('~'), 'casadi-py27-np1.9.1-v3.0.0'))
from casadi import *

import time

import matplotlib.pyplot as plt

from ECSA.Control import Fourier
from ECSA.System_base import Monocycle, KinematicCar
from ECSA.Algorithm import LiftedNewtonProritarian
import ECSA.API.Visualization

def yd(t):
    R = 1
    w = 1
    orientation = (w * t) - np.pi / 2
    return vertcat(R * np.cos(orientation), R * np.sin(orientation) + R, orientation + np.pi / 2)

def generete_trajectory(opts):
    opts['ydt'] = [0,0,0]
    for t in numpy.linspace(0,opts['T'],4*314):
        if t!=0:
            opts['ydt'] = horzcat(opts['ydt'],yd(t))

if __name__ == '__main__':
    print "*****Start"

    opts = {}
    opts['T0'] = 0
    opts['T'] = 2*3.14
    opts['gamma0'] = 0.7
    opts['TOL'] = 10**(-3)
    opts['k_max'] = 50

    opts['q0'] = [0,0.5,0,0]
    opts['x0'] = []
    opts['N'] = 10

    generete_trajectory(opts)
    opts['yd'] = yd

    la1 = [2, 0, 0, 0, 0]
    la2 = [0, 0, 0, 0, 0, 0, 0]
    opts['lambda0'] = vertcat(la1, la2)
    opts['lambda'] = [[len(la1),0],[len(la2),0]]

    opts['viz'] = [False ,False,0.02]

    fourier = Fourier(opts)
    monocycle = Monocycle(fourier)
    kinematic_car = KinematicCar(fourier)

    ecsa = LiftedNewtonProritarian(kinematic_car,opts)

    print "******ECSA Lifted Newton"
    start_time = time.time()
    out = ecsa.run()
    print("--- %s ms ---" % ((time.time() - start_time)*1000))
    print "******End ECSA Lifted Newton"

    if opts['viz'][0] == False:
        ECSA.API.Visualization.showPath(out,opts)
    ECSA.API.Visualization.showTrajectory(out)
    ECSA.API.Visualization.showError(out)
    ECSA.API.Visualization.showControl(out)

    print "*****End"