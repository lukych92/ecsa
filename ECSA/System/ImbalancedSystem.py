from sys import path
path.append(r"../../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.System import System

## System with bounds on state or control
class ImbalancedSystem(System):
    ## The initialization function
    # @param self the object pointer
    # @param system_base object of System_base class
    # @param bounds a dictionary of bounds
    def __init__(self,system_base,bounds):
        System.__init__(self, system_base, bounds)

    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        bounds = self._bounds

        dq = self._system_base.dq(q,u)
        for bound in bounds.keys():
            if bound[0] == 'u':
                n = int(bound[1])
                dq = vertcat(dq,
                             self.g(u[n],
                                    bounds[bound][0],
                                    bounds[bound][1],
                                    bounds[bound][2]
                                    )
                             + self.ru(u[n])
                             )
            if bound[0] == 'q':
                n = int(bound[1])
                dq = vertcat(dq,
                             self.g(q[n],
                                    bounds[bound][0],
                                    bounds[bound][1],
                                    bounds[bound][2]
                                    )
                             + self.rq(q[n])
                             )

        return dq

    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        if self._size_of_bounds == 0:
            return vertcat(self._system_base.y(q,x))
        else:
            return vertcat(self._system_base.y(q,x),q[-self._size_of_bounds:])

    ## regularization for q
    # @param self the object pointer
    # @param q state variable
    def rq(self,q):
        return q**2

    ## regularization for u
    # @param self the object pointer
    # @param u control
    def ru(self,u):
        return arctan(u)**2


	## Function for bound
	# @param self the object pointer
	# @param x q lub u depending on situation
	# @return p(x-ub)+p(-x+lb)
    def g(self,x,lb,ub,alpha):
        return self.p(x-ub,alpha)+self.p(-x+lb,alpha)

	## Mangarasian function p(x,alfa)
	# @param self the object pointer
	# @param x point x
	# @return function value at point x
    def p(self,x,alpha):
        # print alpha
        # print x.is_constant()
        if x.is_constant() and -alpha*x > 1e+02:
            return 0
        else:
            return x + 1/alpha*numpy.log(1 + numpy.exp(-alpha*x))
        # a = x + 1/alpha*numpy.log(1 + numpy.exp(-alpha*x))
        # print a
        # return a
        # return x + 1/alpha*numpy.log(1 + numpy.exp(-alpha*x))
