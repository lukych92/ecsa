__version__ = '1.1.1'

import os, sys
sys.path.append(os.path.join(os.path.dirname(__file__), "casadi-py27-np1.9.1-v3.0.0"))
from casadi import *

import ECSA.API
import ECSA.Algorithm
import ECSA.Control
import ECSA.System
import ECSA.System_base

