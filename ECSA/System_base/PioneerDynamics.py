from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.System_base import System_base

## Class is a definition of pioneer dynamics
#
# Pioneer Dynamics model from Edouard Ivanjko , Toni Petrinic, Ivan Petrovic.MODELLING OF MOBILE ROBOT DYNAMICS.
class PioneerDynamics(System_base):
    ## The initialization function
    # @param self the object pointer
    # @param dim a python dictionary of system dimensions
    # @param control an object of class Control
    def __init__(self,control,opts={}):
        ## dimensions of the monocycle
        self._dim = {
            'n':7,#dim q
            'p':0,#dim x
            'r':3,#dim y
            'm':2,#dim u
        }
        self.m = 28.05 #kg
        self.r = 0.095 #m
        self.b = 0.320 #m
        self.d = 0.0578 #m
        self.I0 = 924*10**(-8) #kg*m^2
        self.IA = 175*10**(-3) #kg*m^2
        self.K = 35*10**(-4) #Nms/rad
        self.A = (self.m*self.r**2/4)+((self.IA+self.m*self.d**2)*self.r**2/self.b**2)+self.I0
        self.B = (self.m*self.r**2/4)-((self.IA+self.m*self.d**2)*self.r**2/self.b**2)
        System_base.__init__(self, self._dim, control)


    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        dksi = numpy.dot(inv(numpy.array([[self.A,self.B], [self.B, self.A]])),
                         numpy.array([[self.K*q[5]+u[0]], [self.K*q[6]+ u[1]]])
                         )
        dq = vertcat(   self.r * (q[5] + q[6]) * cos(q[2]),
                        self.r * (q[5] + q[6]) * sin(q[2]),
                        self.r * (q[5] - q[6]) /self.b,
                        q[5],
                        q[6],
                        dksi[0][0],
                        dksi[1][0]

                )
        return dq

    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        y = vertcat(q[0],
                    q[1],
                    q[2],
                    # q[5],
                    # q[6]
                    )
        return y

    ## Definition of intermediate output for Lifted Newton
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def yi(self,q,x):
        y = vertcat(
                        q[0],
                        q[1]
        )
        return y

    ## Definition of matrix \f$ C \f$ for intermediate point in Lifted Newton
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def Ci(self,q,x):
        fn = Function('fn',
                      [self._q,self._x,],
                      [self.yi(self._q,self._x)],
                      ['q','x'],
                      ['yN']
                     )

        Ci = fn.jacobian('q','yN')

        return Ci(q,x)[0]