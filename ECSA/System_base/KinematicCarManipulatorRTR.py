from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.System_base import System_base


## Class is a definition of kinematic car with RTR manipulator
#
# Mathematical model of the kinematic car with RTR manipulator:
# \f[ \begin{cases}
# \dot{q_1} = u_1 \cos(q_3) \cos(q_4) \\
# \dot{q_2} = u_1 \sin(q_3) \cos(q_4) \\
# \dot{q_3} = u_1 \sin(q_4) \\
# \dot{q_4} = u_2 \\
# y = k(q,x) = \left(
#    \begin{array}{c}
#      q_1 + (l_2 + l_3 \cos(x_3)) (\cos(q_3) + x_1 ) \\
#      q_2 + (l_2 + l_3 \cos (x_3) ) (\sin(q_3 )+ x_1 )  \\
#      x_2 + l_3 sin( x_3 )
#    \end{array}
#  \right)
# \end{cases}
# \f]
#
# \image latex car_rtr.eps "Kinematic car with RTR Manipulator" width=12cm
class KinematicCarManipulatorRTR(System_base):
    ## The initialization function
    # @param self the object pointer
    # @param dim a python dictionary of system dimensions
    # @param control an object of class Control
    def __init__(self,control,opts={}):
        ## dimensions of the monocycle
        self._dim = {
                'n':4,#dim q
                'p':3,#dim x
                'r':3,#dim y
                'm':2,#dim u
            }
        ## length of link 2
        self._l2 = 1
        ## length of link 3
        self._l3 = 1
        System_base.__init__(self, self._dim, control)


    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        dq = vertcat(
                        u[0]*cos(q[2])*cos(q[3]),
                        u[0]*sin(q[2])*cos(q[3]),
                        u[0]*sin(q[3]),
                        u[1]
                )
        return dq

    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        y = vertcat(
                        q[0]+(self._l2 + self._l3*cos(x[2])*cos(q[2]+x[0])),
                        q[1]+(self._l2 + self._l3*cos(x[2])*sin(q[2]+x[0])),
                        x[1]+self._l3*sin(x[2])
                )
        return y
