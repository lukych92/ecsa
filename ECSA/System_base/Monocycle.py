from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.System_base import System_base

## Class is a definition of monocycle
#
# Mathematical model of the monocycle:
# \f[ \begin{cases}
# \dot{q_1} = \cos(q_3)u_1 \\
# \dot{q_2} = \sin(q_3)u_1 \\
# \dot{q_3} = u_2 \\
# y = k(q,x) = (q_1,q_2)
# \end{cases}
# \f]
#
# \image latex monocycle.eps "Monocycle" width=5cm
class Monocycle(System_base):
    ## The initialization function
    # @param self the object pointer
    # @param dim a python dictionary of system dimensions
    # @param control an object of class Control
    def __init__(self,control,opts={}):
        ## dimensions of the monocycle
        self._dim = {
            'n':3,#dim q
            'p':0,#dim x
            'r':3,#dim y
            'm':2,#dim u
        }
        System_base.__init__(self, self._dim, control)

    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        dq = vertcat(
                        cos(q[2])*u[0],
                        sin(q[2])*u[0],
                        u[1]
                )
        return dq

    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        y = vertcat(q[0],
                    q[1],
                    q[2]
                    )
        return y

    ## Definition of intermediate output for Lifted Newton
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def yi(self,q,x):
        y = vertcat(
                        q[0],
                        q[1]
        )
        return y

    ## Definition of matrix \f$ C \f$ for intermediate point in Lifted Newton
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def Ci(self,q,x):
        fn = Function('fn',
                      [self._q,self._x,],
                      [self.yi(self._q,self._x)],
                      ['q','x'],
                      ['yN']
                     )

        Ci = fn.jacobian('q','yN')

        return Ci(q,x)[0]