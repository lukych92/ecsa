from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.ECSASingleShooting import ECSASingleShooting


class Prioritarian(ECSASingleShooting):
    def __init__(self,system,opts):
        ECSASingleShooting.__init__(self, system, opts)


    def ru(self,u):
        ru = 0
        # print "u",type(u)
        for i in range(len(u)):
            ru = ru + arctan(u[i])**2
        return ru


    def dh(self,u,lb,ub,alpha):
        sum = 0


        for i in range(len(u)):
            sum = sum + self.p(u[i]-ub[i],alpha[i])+self.p(-u[i]+lb[i],alpha[i])
        #
        return sum+self.ru(u)

        ## Funkcja Mangarasian p(x,alfa)
        # @param self -- wskaznik na obiekt
        # @param x -- argument funkcji
        # @return wartosc funkcji w punkcie x
    def p(self,x,alpha):
        # print alpha
        # print x.is_constant()
        if x.is_constant() and -alpha*x > 1e+02:
            return 0
        else:
            return x + 1/alpha*numpy.log(1 + numpy.exp(-alpha*x))


    def getKinematicsAndJacobian(self,param,opts):
        ode = param['ode']
        sol = ode[0](x0=opts['q0'],p=param['lambda'])
        x = opts['x0']
        Jt = sol["dxf_dp"]
        sol1 = ode[1](x0=vertcat(opts['q0'],0),p=param['lambda'])

        q = [sol["xf"],
             sol1["xf"][-1]
             ]



        jacobian = [ numpy.dot(param['system'].C(q[0],x), Jt),
                     numpy.dot(param['C_2'],sol1["dxf_dp"])
                     ]

        return q,x,jacobian

    def getInverseJacobian(self,param,opts):
        return [numpy.linalg.pinv(param['J'][0]),numpy.linalg.pinv(param['J'][1])]

    def getError(self,param,opts):
        r_val = param['ru'](x0=0,p=param['lambda'])['xf']
        return [self._system.y(param['q'][0],param['x']) - opts['yd'],
                [float(param['q'][1] - r_val) for m in range(param['Jp'][1].shape[-1])]
                ]

    def getGamma(self,param,opts):
        dl = mtimes(param['Jp'][0],param['error'][0])
        dl1 = mtimes(param['Jp'][1],param['error'][1])

        gmm = 2*opts['gamma0']
        nrm = [numpy.linalg.norm(dl),numpy.linalg.norm(dl1)]


        return [min(sqrt(gmm/nrm[0]),1),min(sqrt(gmm/nrm[1]),1)]

    def getEndogenousConfiguration(self,param,opts):
        sum = 0
        for i in range(len(param['gamma'])):
            P = numpy.eye(param['J'][i].shape[1])
            for j in range(i):
                I = numpy.eye(param['J'][i].shape[1])
                P = mtimes(P, I-mtimes(param['Jp'][i],param['J'][i]))#projection into kernel

            dl = mtimes(param['Jp'][i],param['error'][i])
            sum = sum + param['gamma'][i]*mtimes(P,dl)

        return param['lambda'] - sum,opts['x0']

    def init(self,opts):
        integrator_opts = { 't0':0,
                            'tf':opts['T'],
                            "abstol":1e-6,
                            "reltol":1e-6
                            #                   "cvodes.solver",'ode45'
                            }

        ## the first substask -- motion planning
        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }

        Im = integrator("Im", "cvodes", system, integrator_opts)
        ode = Im.jacobian("p","xf")

        ## the second subtask -- control constraints
        bounds = opts['bounds']

        u = []
        lb = []
        ub = []
        alpha = []

        for key in bounds.keys():
            u.append(self._system._u[int(key[1])])
            lb.append(bounds[key][0])
            ub.append(bounds[key][1])
            alpha.append(bounds[key][2])

        q = SX.sym("q",self._system._q.size1()+1)
        system1 = {'t':self._system._t,
                   'x':q,
                   'p':self._system._control._lambda,
                   'ode':vertcat(self._system.dq(q,
                                                 self._system._u
                                                 ),
                                 self.dh(u,lb,ub,alpha)
                                 )
                   }


        C = numpy.zeros((1,q.size1()))
        C[-1,-1] = 1

        Im1 = integrator("Im1", "cvodes", system1, integrator_opts)
        ode1 = Im1.jacobian("p","xf")

        r = {'t':self._system._t,
             'x':self._system._uclean[0],
             'p':self._system._control._lambda,
             'ode':self.ru(u)
             }
        ru_int = integrator("ru_int","cvodes",r,integrator_opts)

        # algorithm initial conditions
        ##initial condition
        param = {}
        param['error'] = [100,100]

        param['system'] = self._system
        param['ode'] = [ode,ode1]
        param['C_2'] = C
        param['ru'] = ru_int
        return param
