from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from scipy.integrate import odeint
from ECSA.API.ECSASingleShooting import ECSASingleShooting

import numpy


class GeneralLagrangianJacobian(ECSASingleShooting):
    def __init__(self,system,opts):
        ECSASingleShooting.__init__(self, system, opts)

    def getKinematicsAndJacobian(self,param,opts):
        tm = numpy.linspace(0,opts["T"],100)
        n = self._system._dim['n']
        s = self._system._control._dim['s']

        I0 = numpy.zeros((self._system._control._dim['s'],self._system._control._dim['s'])).flatten()
        F0 = numpy.zeros((self._system._dim['n'],self._system._control._dim['s'])).flatten()

        Q0 = numpy.hstack((opts['q0'],I0,F0))
        Q = odeint(self.solver,Q0,tm,args=(self._system,param['lambda'],opts['Q'],opts['R'],opts['S']))

        Q = Q[-1]
        q = Q[0:n]
        x = 0
        I = Q[n:n+s*s].reshape((s,s))
        F = Q[n+s*s:n+s*s+n*s].reshape((n,s))

        C = self._system.C(q,x)

        ##Mobility matrix
        M = numpy.dot(numpy.dot(numpy.dot(numpy.dot(C,F),inv(I)),F.T),C.T)

        #Lagrangian Jacobian inverse
        Jl = numpy.dot(numpy.dot(numpy.dot(inv(I),F.T),C.T),inv(M))

        return q,x,Jl

    def getInverseJacobian(self,param,opts):
        return [param['J']]

    def getError(self,param,opts):
        return [self._system.y(param['q'],param['x']) - opts['yd']]

    def getGamma(self,param,opts):
        dLam = mtimes(param['Jp'][0],param['error'][0])
        gmm = 2*opts['gamma0']
        nrm = norm_2(dLam)
        return [min(sqrt(gmm/nrm),1)]


    def getEndogenousConfiguration(self,param,opts):
        lambda0 =  param['lambda'] - param['gamma'][0]*mtimes(param['Jp'][0],param['error'][0])
        return lambda0,opts['x0']

    def CasadiToNumpy(self,ktmp):
        k1 = numpy.zeros(numpy.shape(ktmp)[0])
        for j in range(numpy.shape(ktmp)[0]):#zamiana z typu casadi na liste
            k1[j] = ktmp[j]
        return k1

    def solver(self,Q0,t,system_base,lambda0,Q,R,S):
        ## initial condition
        n = system_base._dim['n']
        s = system_base._control._dim['s']
        T = system_base._control._T

        P = system_base._control.P(t)
        u = system_base._control.u(t,lambda0)

        q = Q0[0:n]
        I = Q0[n:n+s*s].reshape((s,s))
        F = Q0[n+s*s:n+s*s+n*s].reshape((n,s))

        A = system_base.A(q,u)
        B = system_base.B(q,u)

        ## right-hand side of differentials equations
        dq = self.CasadiToNumpy(system_base.dq(q,u))
        # dI = numpy.dot(numpy.dot(F.transpose(),Q),F) + numpy.dot(numpy.dot(P.T,R),P)
        QSR = numpy.vstack((numpy.hstack((Q,S)),numpy.hstack((S.T,R))))
        dI = numpy.dot(numpy.dot(numpy.hstack((F.T,P.T)),QSR),numpy.vstack((F,P)))
        dF = numpy.dot(B,P)+numpy.dot(A,F)

        ## flattening the matrices
        dI = dI.flatten()
        dF = dF.flatten()


        return numpy.hstack((dq,dI,dF))

    def init(self,opts):
        param = {}
        param['error'] = [100]

        return param
