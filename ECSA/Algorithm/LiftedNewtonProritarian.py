from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from multiprocessing import Process,Queue,Lock

output = Queue()

from ECSA.API.ECSAMultiShooting import ECSAMultiShooting

class LiftedNewtonProritarian(ECSAMultiShooting):
    def __init__(self,system,opts):
        ECSAMultiShooting.__init__(self, system, opts)
        self.yd = opts['yd']
        self.sync = Lock()


    def CasadiToNumpy(self,ktmp):
        k1 = numpy.array(ktmp.T)[0]
        return k1

    def NumpyToCasadi(self,np_array):
        tmp = DM(np_array.shape[0],1)
        for i in range(np_array.shape[0]):
                tmp[i] = np_array[i]
        return tmp


    ## wstawia mala macierz we wskazane pola duzej macierzy
    #    @pre cell i matrix musza byc wektorami
    #    @param row -- wiersz wstawienia
    #    @param column -- kolumna wstawienia
    #    @param cell -- wstawiana macierz
    #    @param cellRow -- ilosc wierszy wstawianej macierzy
    #    @param cellCol -- ilosc kolumn wstawianej macierzy
    #    @param matrix -- macierz do ktorej jest wstawiana wartosc
    #    @param matRow -- ilosc wierzy macierzy
    #    @param matCol -- ilsoc kolumn macierzy
    def setMatrixToCell(self,row, column, cell, cellRow, cellCol, matrix, matRow, matCol):
        colm=0
        ro=-1
        for v in range(len(cell)):
            if(v%cellCol==0):
                ro+=1
                colm=0
            else:
                colm+=1
            y = column*cellCol
            x = (((row*cellRow)+ro)*(matCol))+colm
            matrix[x+y] = cell[v]

    ## Obliczenie rownan rozniczkowych pomiedzy punktami
    #    @param self -- wskaznik na obiekt
    #    @param param --
    def fromPointToPoint(self,m,param,dim):
        opts = {}
        opts["t0"] = self._opts['T0']+(param['Tn']*(m))
        opts["tf"] = self._opts['T0']+(param['Tn']*(m+1))

        q0 = param['s'][m]
        lambda0 = param['lambda'][m]
        yd0 = np.array([0,0,0])

        q_dash = SX.sym("y",dim['r'],1)

        ## system for integrator
        system = {'t':self._system._t,
                  'x':vertcat(self._system._q,q_dash),
                  'p':self._system._control._lambda,
                  'ode':vertcat(self._system.dq(self._system._q,
                                                self._system._u
                                                ),
                                self.g(self._system._t,
                                       self._system._q,
                                       self._system._u,
                                       )
                                )
                  }


        ## integrator
        Im = integrator("Im", "cvodes", system, opts)
        jacobi = Im.jacobian("p","xf")
        sol = jacobi(x0=vertcat(q0,yd0),p=lambda0)

        q = np.array(sol["xf"])[0:self._system._dim['n']]
        vJ = np.array(sol["dxf_dp"][0:self._system._dim['n'],:])

        g = np.array(sol["xf"])[self._system._dim['n']:]
        vJg = np.array(sol["dxf_dp"][self._system._dim['n']:,:])
        # vJg = np.zeros((self._system._dim['r'],self._system._control._dim['s']))

        dintegrator = Im.derivative(1,0)
        for i in range(dim['n']):
            fwd0_x0 = numpy.zeros(dim['n']+dim['r'])
            fwd0_x0[i] = 1
            dhds = numpy.array(dintegrator(der_x0=vertcat(q0,yd0),
                                            der_p=lambda0,
                                            fwd0_x0=fwd0_x0
                                            )['fwd0_xf']).transpose()[0][0:dim['n']]
            if i == 0:
                G = dhds
            else:
                G = numpy.hstack((G, dhds))


        G = G.reshape((dim['n'],dim['n']))
        vG = np.array(G).transpose().flatten()

        H = np.array(q).reshape(1,dim['n'])
        G = H - param['s'][m+1]

        F = np.array(g).reshape(1,dim['r'])

        self.sync.acquire()
        # param['H'][m] = H
        # param['G'][m] = G
        # param['F'][m] = F

        self.sync.release()
        retval = [m,q,H,G,vJ.flatten(),vG,F,vJg.flatten()]

        output.put(retval)

        return retval

    def g(self,t,q,u):
        g = (self._system.y(q,u) - self.yd(t))**2
        return g


    def init(self,opts):
        N = opts['N']

        lambd = opts['lambda0']
        for i in range(1,N):
            lambd = numpy.hstack((lambd,opts['lambda0']))
        lambd = lambd.T

        Tn = (opts['T']-opts['T0'])/N #czas pomiedzy pkt posrednimi

        ##macierzy H
        # wymiar N x n
        H = numpy.zeros((N,self._system._dim['n']))
        G = numpy.ones((N+1,self._system._dim['n']))*100
        F = numpy.zeros((N,self._system._dim['r']))

        ##macierz dHds(s,lambda)
        # wymiar nN x nN
        dHds = numpy.zeros(self._system._dim['n']*N*self._system._dim['n']*N)

        ##macierz dHdlambda(s,lambda) -- wersja wektorowa
        # wymiar nN x mMN
        vdHdlambd = numpy.zeros(N*self._system._dim['n']*N*self._system._control._dim['s'])


        ## dFdlambda(lambda) -- vector version
        # rN x mMN
        vdFdlambda = numpy.zeros(N*self._system._dim['r']*N*self._system._control._dim['s'])

        ## yd(ti)



        n = int(opts['ydt'].size()[1]/N)
        tmp = opts['ydt'][:,n-1::n]
        yd = []
        for i in range(N):
            tmp1 = []
            for j in range(tmp.size()[0]):
                tmp1.append(float(tmp[j,i]))
            yd.append(tmp1)


        # print opts['ydt'][2,-1]
        #
        # yd[N-1].append(float(opts['ydt'][2,-1]))
        #
        # print yd

        r = 0
        for m in range(numpy.shape(yd)[0]):
            r = r + numpy.shape(yd[m])[0]

        opts['s'] = numpy.zeros((N,self._system._dim['n']))
        for m in range(len(yd)):
            for n in range(len(yd[m])):
                opts['s'][m][n]  = yd[m][n]


        param = {}
        param['k_vect'] = []
        param['step'] = 0
        param['error'] = [100]
        param['system'] = self._system
        param['lambda'] = lambd

        param['Tn'] = Tn
        param['N'] = N

        param['H'] = H
        param['G'] = G
        param['F'] = F
        param['dHds'] = dHds
        param['vdHdlambda'] = vdHdlambd
        param['vdFdlambda'] = vdFdlambda
        param['r'] = r
        param['s'] = opts['s']
        return param

    def getJacobianAndError(self,param,opts):
        param['e'] = []
        param['deds'] = numpy.zeros(param['r']*param['N']*self._system._dim['n'])
        param['s'] = numpy.vstack((opts['q0'],param['s']))

        processes = [Process(target=self.fromPointToPoint,args=(x,
                                                                param,
                                                                self._system._dim
                                                                )
                             )
                     for x in range(param['N'])
                     ]

        # Run processes
        for p in processes:
            p.start()


        results = [output.get() for p in processes]
        results.sort()
        #
        # Exit the completed processes
        for p in processes:
            p.join()


        # results = []
        #
        # for m in range(param['N']):
        #     results.append(self.fromPointToPoint(m,param,self._system._dim))
            # print results[m]


        for m in range(param['N']):
            param['H'][m] = results[m][2]
            param['G'][m] = results[m][3]
            param['F'][m] = results[m][6]
            ## wektor jakobianu z Q
            vJ = results[m][4]
            vJg = results[m][7]
            ## wstawienie macierzy J do macierzy dHdlambda
            self.setMatrixToCell(m,
                                 m,
                                 vJ,
                                 self._system._dim['n'],
                                 self._system._control._dim['s'],
                                 param['vdHdlambda'],
                                 param['N']*self._system._dim['n'],
                                 param['N']*self._system._control._dim['s']
                                 )
            self.setMatrixToCell(m,
                                 m,
                                 vJg,
                                 self._system._dim['r'],
                                 self._system._control._dim['s'],
                                 param['vdFdlambda'],
                                 param['N']*self._system._dim['r'],
                                 param['N']*self._system._control._dim['s']
                                 )
            if m>0:
                ## wektor Gamma z Q
                vG = results[m][5]
                ## wstawienie macierzy Gamma do macierzy dHds
                self.setMatrixToCell(m,
                                     m-1,
                                     vG,
                                     self._system._dim['n'],
                                     self._system._dim['n'],
                                     param['dHds'],
                                     self._system._dim['n']*param['N'],
                                     self._system._dim['n']*param['N']
                                     )
            if m != param['N']-1:
                ## Calculate tast space error (err)
                param['e'].extend(self.CasadiToNumpy(self._system.y(param['s'][m+1],0)) - self.CasadiToNumpy(self.yd((param['Tn']*(m+1))+self._opts['T0'])))
                ## Calculate respective derivative (deds)
                #  deds -- wymiar r x nN
                C = numpy.array(self._system.C(param['H'][m],0))
                tmpRow = numpy.shape(C)[0]
                tmpCol = numpy.shape(C)[1]
                C = C.flatten()
                self.setMatrixToCell(m,
                                     m,
                                     C,
                                     tmpRow,
                                     tmpCol,
                                     param['deds'],
                                     param['r'],
                                     param['N']*self._system._dim['n']
                                     )
            else:
                ## Calculate tast space error (err)
                #  obliczenie bledu
                param['e'].extend(self.CasadiToNumpy(self._system.y(param['s'][m+1],0)) - self.CasadiToNumpy(self.yd((param['Tn']*(m+1))+self._opts['T0'])))


                ## Calculate respective derivative (deds)
                #  deds -- wymiar r x nN
                C = numpy.array(self._system.C(param['H'][m],0))
                # tmpRow = numpy.shape(C)[0]
                tmpCol = numpy.shape(C)[1]
                C = C.flatten()
                self.setMatrixToCell(m,
                                     m,
                                     C,
                                     tmpRow ,
                                     tmpCol,
                                     param['deds'],
                                     param['r'],
                                     param['N']*self._system._dim['n']
                                     )

        k = self.CasadiToNumpy(self._system.y(param['s'][-1],0))

        for m in range(0,len(param['G'][param['N']])):
        # for m in range(0,len(k)):
            if m < len(k):
                param['G'][param['N'],m] = (k - self.yd(opts['T']))[m]
            else:
                param['G'][param['N'], m] = 0

        # print param['G']

        ## macierz pkt posrednich
        #  wykorzystywana do wyrysowania pkt posrednich
        for m in range(self._system._dim['n']):
            param['s'] = numpy.delete(param['s'], 0)
        param['s'] = param['s'].reshape(param['N'], self._system._dim['n'])


        param['dHdlambda'] = param['vdHdlambda'].reshape(param['N']*self._system._dim['n'],
                                                       param['N']*self._system._control._dim['s'])
        param['deds'] = param['deds'].reshape(param['r'], param['N']*self._system._dim['n'])
        param['dHds'] = param['dHds'].reshape(self._system._dim['n']*param['N'],self._system._dim['n']*param['N'])
        param['dFdlambda'] = param['vdFdlambda'].reshape(param['N']*self._system._dim['r'],
                                                       param['N']*self._system._control._dim['s'])


        return param

    def getHelpVariable(self,param,opts):
        ## Set helper variable
        # M  -- wymiar nN x nN
        M = -np.linalg.inv(np.array(param['dHds']-numpy.eye(self._system._dim['n']*param['N'])))
        param['dHds'] = param['dHds'].flatten()



        ## Calculate z,Z,w,W
        # z -- nN x 1
        # Z -- nN x mMN
        # w -- n x 1
        # W -- n x mMN
        # X -- mMN x mMN
        # v -- mMN x N
        Hs = param['H'] -param['s']
        Hs = Hs.flatten()

        param['z'] = numpy.dot(M,Hs)
        param['Z'] = numpy.dot(M,param['dHdlambda'])
        param['w'] = param['e'] + numpy.dot(param['deds'],param['z'])
        param['W'] = numpy.dot(param['deds'],param['Z'])
        param['X'] = numpy.eye(param['N']*self._system._control._dim['s'])-numpy.dot(numpy.linalg.pinv(param['W']),param['W'])
        param['v'] = numpy.dot(numpy.linalg.pinv(param['dFdlambda']),param['F'].flatten())

        return param

    def getNewtonStepLength(self,param,opts):
        ## Calculate Newton step lenght
        # Wpinv -- wymiar mMN x n
        # dlambda -- wymiar mMN x 1
        # ds -- wymiar nN x 1
        dlam = - numpy.dot(pinv(param['W']),param['w'])-numpy.dot(param['X'],param['v'])
        return [dlam,
                param['z'] + numpy.dot(param['Z'],dlam)
                ]

    def getGamma(self,param,opts):
        ## Calculate damping factors
        gmm = 2*opts['gamma0']
        return [float(min(sqrt(gmm/norm_2(param['dlambda'][:,None])),1)),
                float(min(sqrt(gmm/norm_2(param['ds'])),1))
                ]

    def getNewtonStep(self,param,opts):
        # Perform Newton step
        # lambd -- wymiar mMN
        # s -- wymiar nN
        param['lambda'] = param['lambda'].flatten()
        param['s'] = param['s'].flatten()

        param['lambda'] = param['lambda'] + param['gamma'][0] * param['dlambda']
        param['s'] = param['s'] + param['gamma'][1] * param['ds']

        param['lambda'] = param['lambda'].reshape(param['N'],self._system._control._dim['s'])
        param['s'] = param['s'].reshape(param['N'],self._system._dim['n'])

        return param['lambda'],param['s']

    def getError(self,param,opts):
        return [param['G'].flatten(),param['F'].flatten()]