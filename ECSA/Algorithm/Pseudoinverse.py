from sys import path
path.append(r"./casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.ECSASingleShooting import ECSASingleShooting


class Pseudoinverse(ECSASingleShooting):
    def __init__(self,system,opts):
        ECSASingleShooting.__init__(self, system, opts)

    def getKinematicsAndJacobian(self,param,opts):
        ode = param['ode']
        sol = ode(x0=opts['q0'],p=param['lambda'])
        q = sol["xf"]
        x = param['x']
        Jt = sol["dxf_dp"]
        J = numpy.dot(param['system'].C(q,x), Jt)
        J = horzcat(J,param['system'].D(q,x))
        return q,x,J

    def getInverseJacobian(self,param,opts):
        return [numpy.linalg.pinv(param['J'])]

    def getError(self,param,opts):
        return [param['system'].y(param['q'],param['x']) - opts['yd']]


    def getGamma(self,param,opts):
        dLam = mtimes(param['Jp'][0],param['error'][0])
        gmm = 2*opts['gamma0']
        nrm = norm_2(dLam)
        return [min(sqrt(gmm/nrm),1)]


    def getEndogenousConfiguration(self,param,opts):
        X =  vertcat(param['lambda'],param['x']) - param['gamma'][0]*mtimes(param['Jp'][0],param['error'][0])
        return X[:self._system._control._dim['s']],X[self._system._control._dim['s']: ]


    def init(self,opts):
        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }

        integrator_opts = { 't0':0,
                            'tf':self._opts["T"],
                            "abstol":1e-6,
                            "reltol":1e-6
        #                   "cvodes.solver",'ode45'
                        }

        Im = integrator("Im", "cvodes", system, integrator_opts)
        ode = Im.jacobian("p","xf")


        # algorithm initial conditions
        ##initial condition
        param = {}
        param['k_vect'] = []
        param['step'] = 0
        param['error'] = [100]
        param['system'] = self._system
        param['lambda'] = self._opts['lambda0']
        param['x'] = self._opts['x0']
        param['ode'] = ode
        return param