from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.ECSASingleShooting import ECSASingleShooting


class Imbalanced(ECSASingleShooting):
    def __init__(self,system,control,opts):
        ECSASingleShooting.__init__(self, system, opts)

    def getKinematicsAndJacobian(self,param,opts):
        ode = param['ode']
        sol = ode(x0=opts['q0'],p=param['lambda'])
        q = sol["xf"]
        x = opts["x0"]
        Jt = sol["dxf_dp"]
        J = numpy.dot(param['system'].C(q,x), Jt)# + numpy.dot(system_base.D(q,x),x0)
        return q,x,J


    def getInverseJacobian(self,param,opts):
        return [numpy.linalg.pinv(param['J'])]


    def getError(self,param,opts):
        ## extra part with regularization
        q_bounds = 0
        u_bounds = 0
        for key in param['system']._bounds.keys():
            if key[0] == 'u':
                u_bounds = u_bounds + 1
            if key[0] == 'q':
                q_bounds = q_bounds + 1

        ru= param['system'].ru_int(x0=[0 for x in range(u_bounds)],p=param['lambda'])['xf'][-1]
        rq= param['system'].rq_int(x0=[0 for x in range(q_bounds)],p=param['lambda'])['xf'][-1]

        r = [float(ru) for m in range(u_bounds)]
        r = numpy.hstack((r,[float(rq) for n in range(q_bounds)]))

        yd = vertcat(opts['yd'],r)

        return [param['system'].y(param['q'],param['x']) - yd]

    def getGamma(self,param,opts):
        dLam = mtimes(param['Jp'][0],param['error'][0])
        gmm = 2*opts['gamma0']
        nrm = norm_2(dLam)
        return [min(sqrt(gmm/nrm),1)]

    def getEndogenousConfiguration(self,param,opts):
        lambda0 = param['lambda'] - param['gamma'][0]*mtimes(param['Jp'][0],param['error'][0])
        return lambda0,opts['x0']

    def init(self,opts):
        system = {'t':self._system._t,
                  'x':self._system._q,
                  'p':self._system._control._lambda,
                  'ode':self._system.dq(self._system._q,
                                        self._system._u
                                        )
                  }

        integrator_opts = { 't0':0,
                            'tf':self._opts["T"],
                            "abstol":1e-6,
                            "reltol":1e-6
        #                   "cvodes.solver",'ode45'
                        }

        Im = integrator("Im", "cvodes", system, integrator_opts)
        ode = Im.jacobian("p","xf")


        # algorithm initial conditions
        ##initial condition
        param = {}
        param['error'] = [100]

        param['system'] = self._system
        param['ode'] = ode
        return param