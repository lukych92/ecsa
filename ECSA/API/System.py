from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

## System class
#
# The system is defined as extended system of System_base class and
# is defined as:
# \f[
# \begin{cases}
# \dot{q} = f(q,u)\\
# \dot{q}_{n+1} = f_1(q_1)\\
# \vdots \\
# \dot{q}_{n+n} = f_1(q_n)\\
# \dot{q}_{2n+1} = f_2(u_1)\\
# \vdots \\
# \dot{q}_{2n+m} = f_2(u_m)\\
# \hat{y} = \hat{k}(\hat{q},x) =(k(q,x),q_{n+1},\hdots,q_{2n+m})\\
# \end{cases}
# \f]
# where:
# - \f$ q \in \mathbb{R}^n\f$
# - \f$ \hat{q} = (q, q_{n+1} ,\ldots , q_{2n+m} ) \in \mathbb{R}^{\hat{n}}, \quad \hat{n} = 2n+m\f$
# - \f$ \hat{y} =(y, y_{r+1} ,\ldots, y_{r+n+m} ) \in \mathbb{R}^{\hat{r}}, \quad \hat{r} = r+n+m\f$
#
# The extended system can be defined as:
# \f[
# \begin{cases}
# \dot{\hat{q}} = \hat{f}(\hat{q},u)\\
# \hat{y} = \hat{k}(\hat{q},x) =(k(q,x),q_{n+1},\hdots,q_{2n+m})\\
# \end{cases}
# \f]
#
class System:
    ## The initialization function
    # @param self the object pointer
    # @param system_base object of System_base class
    # @param bounds a dictionary of bounds
    def __init__(self,system_base,bounds):
        ## object of System_base class
        self._system_base = system_base
        ##  object of Control class
        self._control = system_base._control
        ## dictionary of bounds
        self._bounds = bounds
        ## size of the bounds
        self._size_of_bounds = len(self._bounds)
        ## dimension of the system
        self._dim = system_base._dim
        self._dim['n'] = self._dim['n']+self._size_of_bounds
        ## symbolic vector of state variables
        self._q = SX.sym('q',self._dim['n'])
        ## symbolic vector of joint positions
        self._x = SX.sym('x',self._dim['p'])
        ## symbolic vector of nonparametrized control
        self._uclean = SX.sym('u',self._dim['m'])
        ## symbolic time
        self._t = SX.sym('t')
        ## symbolic vector of parametrized control
        self._u = self._control.u(self._t,
                                  self._control._lambda
                                  )


        opts = {'grid':numpy.linspace(0.0, self._control._T, 1000)}
        ru = {'t':self._t,
              'x':self._uclean,
              'p':self._control._lambda,
              'ode':self.ru(self._u)}

        ## CasADi integrator for u regularization
        self.ru_int = integrator("ru_int","cvodes",ru,opts)

        rq = {'t':self._t,
              'x':self._system_base._q,
              'p':self._control._lambda,
              'ode':self.rq(self._q)}

        ## CasADi integrator for q regularization
        self.rq_int = integrator("ru_int","cvodes",ru,opts)

        f = Function('f',
                     [self._q,self._x,self._uclean],
                     [self.dq(self._q,self._uclean),self.y(self._q,self._x)],
                     ['q','x','u'],
                     ['dq','k']
                     )
        ## symbolic matrix A
        self._A = f.jacobian('q','dq')
        ## symbolic matrix D
        self._B = f.jacobian('u','dq')
        ## symbolic matrix C
        self._C = f.jacobian('q','k')
        ## symbolic matrix D
        self._D = f.jacobian('x','k')

    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        raise "Define dq(q,u)"

    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        raise "Define y(q,x)"

    ## Give matrix \f$A\f$
    #
    # Matrix \f$A\f$ is defined as
    # \f[ A(t) = \frac{\partial \hat{f}(\hat{q}(t),u(t))}{\partial \hat{q}} \f]
    # @param self the object pointer
    # @param q state variables
    # @param u control
    # @retval A Matrix \f$A\f$
    def A(self,q,u):
        return self._A(q,0,u)[0]

    ## Give matrix \f$B\f$
    #
    # Matrix \f$B\f$ is defined as
    # \f[ B(t) = \frac{\partial  \hat{f}(\hat{q}(t),u(t))}{\partial u} \f]
    # @param self the object pointer
    # @param q state variables
    # @param u control
    # @retval B Matrix \f$B\f$
    def B(self,q,u):
        return self._B(q,0,u)[0]

    ## Give matrix \f$C\f$
    #
    # Matrix \f$C\f$ is defined as
    # \f[ C(t,x) = \frac{\partial \hat{k}(\hat{q}(t),x)}{\partial \hat{q}} \f]
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    # @retval C Matrix \f$C\f$
    def C(self,q,x):
        return self._C(q,x,0)[0]

    ## Give matrix \f$D\f$
    #
    # Matrix \f$D\f$ is defined as
    # \f[ D(t,x) = \frac{\partial \hat{k}(\hat{q}(t),x)}{\partial x} \f]
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    # @retval D Matrix \f$D\f$
    def D(self,q,x):
        return self._D(q,x,0)[0]

    ## regularization for u
    # @param self the object pointer
    # @param u control
    def ru(self,u):
        raise "Define r(u)"

    ## regularization for q
    # @param self the object pointer
    # @param q state variable
    def rq(self,q):
        raise "Define r(q)"

    ## function for bound
    # @param self the object pointer
    def g(self):
        raise "Define g(q)"
