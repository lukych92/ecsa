from sys import path
path.append(r"../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

## System_base class
#
# A system base is defined as a control affine system
# \f[
# \dot{q} = f(q,u), \quad y = k(q,x),
# \f]
# where:
# - \f$ q \in \mathbb{R}^n\f$, \f$u \in \mathbb{R}^m\f$, \f$x \in \mathbb{R}^p\f$
# - \f$\dot{q}\f$ is a function of state evolution
# - \f$y\f$ is a output function
#
class System_base:
    ## The initialization function
    # @param self the object pointer
    # @param dim a python dictionary of system dimensions
    # @param control an object of class Control
    def __init__(self,dim,control):
        ##  object of Control class
        self._control = control
        ## symbolic vector of state variables
        self._q = SX.sym('q',dim['n'])
        ## symbolic vector of joint positions
        self._x = SX.sym('x',dim['p'])
        ## symbolic vector of nonparametrized control
        self._uclean = SX.sym('u',dim['m'])
        ## symbolic time
        self._t = SX.sym('t')
        ## symbolic vector of parametrized control
        self._u = self._control.u(self._t,
                                  self._control._lambda
                                  )

        f = Function('f',
                     [self._q,self._x,self._uclean],
                     [self.dq(self._q,self._uclean),self.y(self._q,self._x)],
                     ['q','x','u'],
                     ['dq','k']
                     )
        ## symbolic matrix A
        self._A = f.jacobian('q','dq')
        ## symbolic matrix D
        self._B = f.jacobian('u','dq')
        ## symbolic matrix C
        self._C = f.jacobian('q','k')
        ## symbolic matrix D
        self._D = f.jacobian('x','k')

    ## Definition of state evolution
    # @param self the object pointer
    # @param q state variables
    # @param u control
    def dq(self,q,u):
        raise "Define dq(q,u)"

    ## Definition of output
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    def y(self,q,x):
        raise "Define y(q,x)"

    ## Give matrix \f$A\f$
    #
    # Matrix \f$A\f$ is defined as
    # \f[ A(t) = \dfrac{\partial f(q(t),u(t))}{\partial q} \f]
    # @param self the object pointer
    # @param q state variables
    # @param u control
    # @retval A Matrix \f$A\f$
    def A(self,q,u):
        return self._A(q,0,u)[0]

    ## Give matrix \f$B\f$
    #
    # Matrix \f$B\f$ is defined as
    # \f[ B(t) = \frac{\partial f(q(t),u(t))}{\partial u} \f]
    # @param self the object pointer
    # @param q state variables
    # @param u control
    # @retval B Matrix \f$B\f$
    def B(self,q,u):
        return self._B(q,0,u)[0]

    ## Give matrix \f$C\f$
    #
    # Matrix \f$C\f$ is defined as
    # \f[ C(t,x) = \frac{\partial k(q(t),x)}{\partial q} \f]
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    # @retval C Matrix \f$C\f$
    def C(self,q,x):
        return self._C(q,x,0)[0]

    ## Give matrix \f$D\f$
    #
    # Matrix \f$D\f$ is defined as
    # \f[ D(t,x) = \frac{\partial k(q(t),x)}{\partial x} \f]
    # @param self the object pointer
    # @param q state variables
    # @param x joint position
    # @retval D Matrix \f$D\f$
    def D(self,q,x):
        return self._D(q,x,0)[0]