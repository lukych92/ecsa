from sys import path
path.append(r"../../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

import math

from ECSA.API.Control import Control

## Interface for Jacobi polynomial classes like Legendre, Gagenbauer, Chebyshev \cite TchonJakubiak2004
class Jacobi(Control):
    ## The initialization function
    # @param self the object pointer
    # @param opts a python dictionary with options like time horizon \f$T\f$
    def __init__(self,alpha,beta,opts):
        Control.__init__(self, opts)
        self._alpha = alpha
        self._beta = beta

    def P(self,t):
        return self.getPmatrix(t)

    ## Gives a value from jacobi polynomial
    #
    # \f$
    # J_k^{\alpha,\beta}(t) = \dfrac{(-1)^k}{k!} \left( \dfrac{2}{T^2} \right)^k
    # (T-t)^{-\alpha}\; t^{-\beta} \dfrac{d^k}{dt^k} \left( (T-t)^{\alpha+k}\; t^{\beta+k} \right),
    # \quad \alpha,\beta > -1, \quad k \geq 0
    # \f$
    def JacobiPolynomial(self,t,T,k,alpha,beta):
        t1 = SX.sym('t1')
        diff = ((T-t1)**(alpha+k))*((t1)**(beta+k))
        result = ((T-t)**(alpha+k))*((t)**(beta+k))
        A = ((-1)**k/float(math.factorial(k)))
        A = A*((2/T**2)**k)*((T-t)**(-alpha))*((t)**(-beta))

        for i in range(1,k):
            f = Function('f',
                         [t1],
                         [diff],
                         ['t'],
                         ['diff']
                         )

            d = f.jacobian('t','diff')
            diff =  d(t1)[0]
            result = d(t)[0]



        # for i in range(k):
        #     diff = jacobian(diff, t1)
        # Jn = Function('Jn',[t1], [diff])

        return A*result

    ## Give vector from jacobi base
	# @param self the object pointer
	# @param t time
	# @return vector from jacobi base
    def getJacobiVect(self,t,number_of_elements,from_element=0):
        vect = self.JacobiPolynomial(t,self._T,0+from_element,self._alpha,self._beta)

        for i in range(1,number_of_elements):
            vect = horzcat(vect,self.JacobiPolynomial(t,
                                                      self._T,
                                                      i+from_element,
                                                      self._alpha,
                                                      self._beta)
                           )
        return vect


    def getPmatrix(self,t):
        zero_vect = 0

        for i in range(1,self._dim['s']):
            zero_vect = horzcat(zero_vect,0)

        P = self.getJacobiVect(t,
                               self._opts['lambda'][0][0],
                               self._opts['lambda'][0][1]
                               )
        for i in range(1,len(self._opts['lambda'])):
            vect = self.getJacobiVect(t,
                                       self._opts['lambda'][i][0],
                                       self._opts['lambda'][i][1]
                                       )
            P = horzcat(P,zero_vect,vect)

        P = reshape(P,self._dim['s'],self._dim['m']).T
        return P



