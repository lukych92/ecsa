from sys import path
path.append(r"../../casadi-py27-np1.9.1-v3.0.0")
from casadi import *

from ECSA.API.Control import Control

## Fourier base class
class Fourier(Control):
    ## The initialization function
    # @param self the object pointer
    # @param opts a python dictionary with options like time horizon \f$T\f$
    def __init__(self,opts):
        Control.__init__(self, opts)

    def P(self,t):
        return self.getPmatrix(t)

    ## Gives a value of sine function
	# @param self the object pointer
	# @param n harmonic
	# @param t time
	# @return sine value
    def getsin(self,n,t):
        w = 2*pi/self._T
        return sin(w*n*(t-self._Tb))

    ## Gives a value of cosine function
	# @param self the object pointer
	# @param n harmonic
	# @param t time
	# @return cosine value
    def getcos(self,n,t):
        w = 2*pi/self._T
        return cos(w*n*(t-self._Tb))


    def getFourierVect(self,t,number_of_elements,from_element=0):
        k = from_element
        if from_element%2:
            tmp_vect = self.getsin(k,t)
        else:
            tmp_vect = self.getcos(k,t)
            k = k + 1



        for i in range (from_element + 1,number_of_elements+from_element):
            # print i,tmp_vect
            if i%2:
                tmp_vect = horzcat(tmp_vect,self.getsin(k,t))
            else:
                tmp_vect = horzcat(tmp_vect,self.getcos(k,t))
                k=k+1
        return tmp_vect

    def getPmatrix(self,t):
        zero_vect = 0

        for i in range(1,self._dim['s']):
            zero_vect = horzcat(zero_vect,0)


        P = self.getFourierVect(t,
                                self._opts['lambda'][0][0],
                                self._opts['lambda'][0][1]
                                )
        for i in range(1,len(self._opts['lambda'])):
            vect = self.getFourierVect(t,
                                       self._opts['lambda'][i][0],
                                       self._opts['lambda'][i][1]
                                       )
            P = horzcat(P,zero_vect,vect)

        P = reshape(P,self._dim['s'],self._dim['m']).T
        return P



